import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @Input() userData = {
    fullname: '',
    dob: '',
    address: '',
    sex: '',
    phone_no: '',
    email: '',
    password: '',
  };

  addUser: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    public user_serv: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.addUser = this.formBuilder.group({
      fullname: ['', Validators.required],
      dob: ['', Validators.required],
      address: ['', Validators.required],
      sex: ['', Validators.required],
      phone_no: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  saveUser() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addUser.invalid) {
      return;
    } else {
      // alert("Submitted");
      this.user_serv.addUser(this.userData).subscribe((result) => {
        this.router.navigate(['/users/']);
      }, (err) => {
        console.log(err);
      });
    }

  }

}
