import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth-guard';
import { ProductsComponent } from './products/products.component'
import { AddProductComponent } from './add-product/add-product.component';
import { UsersComponent } from './users/users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { OrdersComponent } from './orders/orders.component';
import { AddOrderComponent } from './add-order/add-order.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { AddSupplierComponent } from './add-supplier/add-supplier.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { EditSupplierComponent } from './edit-supplier/edit-supplier.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  // {
  //   path: '', component: LoginComponent, data: { title: 'Login' }, canActivate: [AuthGuard]
  // },
  // { path: '',redirectTo: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  {
    path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' }, canActivate: [AuthGuard]
  },
  {
    path: 'products', component: ProductsComponent, data: { title: 'All Products' }, canActivate: [AuthGuard]
  },
  {
    path: 'addproduct', component: AddProductComponent, data: { title: 'Add Product' }, canActivate: [AuthGuard]
  },
  {
    path: 'editproduct/:id', component: EditProductComponent, data: { title: 'Update Product' }, canActivate: [AuthGuard]
  },
  {
    path: 'users', component: UsersComponent, data: { title: 'All Users' }, canActivate: [AuthGuard]
  },
  {
    path: 'adduser', component: AddUserComponent, data: { title: 'Add User' }, canActivate: [AuthGuard]
  },
  {
    path: 'edituser/:id', component: EditUserComponent, data: { title: 'Edit User' }, canActivate: [AuthGuard]
  },
  {
    path: 'orders', component: OrdersComponent, data: { title: 'All Orders' }, canActivate: [AuthGuard]
  },
  {
    path: 'addorder', component: AddOrderComponent, data: { title: 'Add Order' }, canActivate: [AuthGuard]
  },
  {
    path: 'editorder/:id', component: EditOrderComponent, data: { title: 'Edit Order' }, canActivate: [AuthGuard]
  },
  {
    path: 'suppliers', component: SuppliersComponent, data: { title: 'Suppliers' }, canActivate: [AuthGuard]
  },
  {
    path: 'addsupplier', component: AddSupplierComponent, data: { title: 'Add Supplier' }, canActivate: [AuthGuard]
  },
  {
    path: 'editsupplier/:id', component: EditSupplierComponent, data: { title: 'Edit Supplier' }, canActivate: [AuthGuard]
  },
  {
    path: 'login', component: LoginComponent, data: { title: 'Login' }
  },
  {
    path: 'sign-up', component: RegisterComponent, data: { title: 'Register' }
  },
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
