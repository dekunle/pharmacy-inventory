import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product-service';
// import { OrderModule } from 'ngx-order-pipe';
// import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: any = [];
  p: number = 1;

  constructor(
    public product: ProductService
  ) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.products = [];
    this.product.getProducts().subscribe((data: [{}]) => {
      console.log(data);
      this.products = data;
    });
  }

  delete(id) {
    this.product.deleteProduct(id)
      .subscribe(res => {
        this.getProducts();
      }, (err) => {
        console.log(err);
      }
      );
  }

}
