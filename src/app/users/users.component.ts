import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user-service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any = [];
  p: number = 1;

  constructor(
    public user_rest: UserService
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    this.user_rest.getUsers().subscribe((data: [{}]) => {
      console.log(data);
      this.users = data;
    });
  }

  delete(id) {
    this.user_rest.deleteUser(id)
      .subscribe(res => {
        this.getUsers();
      }, (err) => {
        console.log(err);
      }
      );
  }

}
