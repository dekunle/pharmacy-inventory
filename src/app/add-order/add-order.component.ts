import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { OrderService } from '../services/order-service';
import { ProductService } from '../services/product-service';
import * as moment from 'moment';
import * as uuid from 'uuid';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {

  @Input() orderData = {
    product_id: '',
    discount: 0,
    quantity: 1,
    customer_name: '',
    bill_no: uuid.v4(),
    date: moment().format("MMM DD, YYYY"),
    payment_type: 'cash',
    price: 0,
    sold_by: localStorage.getItem('userID')
  };

  products: any = [];
  product: any;
  unit_price: 0;
  instock: 0;

  constructor(
    public order_rest: OrderService,
    public product_rest: ProductService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  addOrder: FormGroup;
  submitted: boolean = false;


  public error: string;

  saveOrder() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addOrder.invalid) {
      return;
    } else {
      // alert("Submitted");
      this.order_rest.addOrder(this.orderData).subscribe((result) => {
        // this.router.navigate(['/orders/']);
        const instock = this.instock - result.quantity;
        // console.log(instock);
        this.product_rest.updateProduct(result.product_id, {
          quantity: instock
        }).subscribe((data) => {
          // console.log(data);
          alert("Successful");
          
        })
      }, (err) => {
        console.log(err);
      });
    }

  }

  fetchProductDetails() {
    const product = this.orderData.product_id;
    this.product_rest.getProduct(product).subscribe((data) => {
      // console.log(data.discount);
      const price = data.unit_price
      const quantity = data.quantity
      this.orderData.discount = data.discount;
      this.unit_price = price;
      this.instock = quantity;
      if (this.instock < 10) {
        alert("Only " + this.instock + " remains in stock");
      } else if (this.instock === 0) {
        alert(data.name + " is no longer in stock");
      }
    }, (err) => {
      console.log(err);
    });
  }

  onKey() {
    const quantity = this.orderData.quantity
    const price = this.unit_price
    const discount = this.orderData.discount

    const final_price = price - discount;
    this.orderData.price = quantity * final_price
    console.log(this.orderData.price);

  }

  getProducts() {
    this.products = [];
    this.product_rest.getProducts().subscribe((data: {}) => {
      // console.log(data);
      this.products = data;
    }, (err) => {
      console.log(err);
    });
  }

  ngOnInit() {
    this.getProducts();
    this.addOrder = this.formBuilder.group({
      product_id: ['', Validators.required],
      quantity: ['', Validators.required],
      customer_name: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  get form() { return this.addOrder.controls; }

}
