import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../services/user-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @Input() userData: any = {
    fullname: '',
    dob: '',
    address: '',
    sex: '',
    phone_no: '',
    email: '',
    password: '',
  };

  editUser: FormGroup;
  submitted: boolean = false;

  public error: string;

  constructor(
    private formBuilder: FormBuilder,
    public user_serv: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user_serv.getUser(this.route.snapshot.params['id']).subscribe((data: {}) => {
      // console.log(data);
      this.userData = data;
    });
    this.editUser = this.formBuilder.group({
      fullname: ['', Validators.required],
      dob: ['', Validators.required],
      address: ['', Validators.required],
      sex: ['', Validators.required],
      phone_no: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  updateUser() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editUser.invalid) {
      return;
    } else {
      this.user_serv.updateUser(this.route.snapshot.params['id'], this.userData).subscribe((result) => {
        this.router.navigate(['/users/']);
      }, (err) => {
        console.log(err);
      });
    }
  }

}
