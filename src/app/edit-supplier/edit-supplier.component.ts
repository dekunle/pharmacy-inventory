import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SupplierService } from '../services/supplier-service';

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.css']
})
export class EditSupplierComponent implements OnInit {

  editSupplier: FormGroup;
  submitted: boolean = false;
  public error: string;

  @Input() supplierData: any = {
    company_name: '',
    contact: '',
    address: ''
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public supplier_serv: SupplierService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.supplier_serv.getSupplier(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.supplierData = data;
    });

    this.editSupplier = this.formBuilder.group({
      company_name: ['', Validators.required],
      contact: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  updateProduct() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editSupplier.invalid) {
      return;
    } else {
      this.supplier_serv.updateSupplier(this.route.snapshot.params['id'], this.supplierData).subscribe((result) => {
        this.router.navigate(['/suppliers/']);
      }, (err) => {
        console.log(err);
      });
    }
  }

}
