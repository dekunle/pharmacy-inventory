import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SupplierService } from '../services/supplier-service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit {

  @Input() supplierData = {
    company_name: '',
    contact: '',
    address: ''
  };

  addSupplier: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    public supplier_serv: SupplierService,
    private router: Router
  ) { }

  ngOnInit() {
    this.addSupplier = this.formBuilder.group({
      company_name: ['', Validators.required],
      contact: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  saveSupplier() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addSupplier.invalid) {
      return;
    } else {
      // alert("Submitted");
      this.supplier_serv.addSupplier(this.supplierData).subscribe((result) => {
        this.router.navigate(['/suppliers/']);
      }, (err) => {
        console.log(err);
      });
    }

  }

}
