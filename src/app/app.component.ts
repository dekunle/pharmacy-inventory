import { Component } from '@angular/core';
import { AuthenticationService } from './services/auth-service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'pharmacy-inventory';
  constructor(
    private auth: AuthenticationService,
    private router: Router
  ) { }

  logout() {
    this.auth.logout();
    this.router.navigate(['login']);
  }
}
