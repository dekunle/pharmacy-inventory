import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from '../services/auth-service';
import { Router } from "@angular/router";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  @Input() loginDetails = {
    email: '',
    password: '',
    strategy: 'local'
  };

  loginForm: FormGroup;
  submitted: boolean = false;
  invalidLogin: boolean = false;

  public error: string;

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    } else {
      this.authService.logMeIn(this.loginDetails).subscribe((result) => {
        this.router.navigate(['/dashboard']);
      }, (err) => {
        if (err.statusText === 'Unauthorized') {
          alert("Invalid credentials. Try again!!!");
        }
        // console.log("Error text", err.statusText);
      });
      // this.authService.login(this.loginForm.controls.email.value,
      //   this.loginForm.controls.password.value, "local")
      //   .pipe(first()).subscribe(
      //     result => {
      //       console.log(result);
      //       // this.router.navigate(['products'])
      //     }
      //   )

    }

  }

  get form() { return this.loginForm.controls; }


  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

}
