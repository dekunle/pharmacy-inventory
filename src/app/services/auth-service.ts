import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST'
    })
};

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) {
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    private handleErrors(error: any) {
        let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        return throwError(error);
    }

    login(email: string, password: string, strategy: string): Observable<boolean> {
        return this.http.post<{ accessToken: string, userData }>('http://localhost:3030/authentication', {
            email: email,
            password: password,
            strategy: strategy
        }, httpOptions)
            .pipe(map(result => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('access_token', result.accessToken);
                localStorage.setItem('userID', result.userData._id);
                // console.log(result);
                return true;
            }
            ), catchError(this.handleError<any>('login')));
    }

    logMeIn(loginDetails): Observable<any> {
        return this.http.post<any>('http://localhost:3030/authentication', JSON.stringify(loginDetails), httpOptions).pipe(
            tap((result) => {
                // console.log(result.userData)
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('access_token', result.accessToken);
                localStorage.setItem('userID', result.userData._id);
                localStorage.setItem('fullname', result.userData.fullname);
            }),
            catchError(this.handleErrors)
        );
    }

    logout() {
        localStorage.removeItem('access_token');
    }

    public get loggedIn(): boolean {
        return (localStorage.getItem('access_token') !== null);
    }
}