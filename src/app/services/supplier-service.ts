import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("access_token")
    })
};

const endpoint = 'http://localhost:3030/';

@Injectable({
    providedIn: 'root'
})
export class SupplierService {

    constructor(private http: HttpClient) { }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    getSuppliers(): Observable<any> {
        return this.http.get(endpoint + 'suppliers', httpOptions).pipe(
            map(this.extractData));
    }

    getSupplier(id): Observable<any> {
        return this.http.get(endpoint + 'suppliers/' + id, httpOptions).pipe(
            map(this.extractData));
    }

    addSupplier(supplier): Observable<any> {
        console.log(supplier);
        return this.http.post<any>(endpoint + 'suppliers', JSON.stringify(supplier), httpOptions).pipe(
            tap((supplier) => console.log(`added supplier w/ id=${supplier._id}`)),
            catchError(this.handleError<any>('addSupplier'))
        );
    }

    updateSupplier(id, supplier): Observable<any> {
        return this.http.patch(endpoint + 'suppliers/' + id, JSON.stringify(supplier), httpOptions).pipe(
            tap(_ => console.log(`updated supplier id=${id}`)),
            catchError(this.handleError<any>('updateSupplier'))
        );
    }

    deleteSupplier(id): Observable<any> {
        return this.http.delete<any>(endpoint + 'suppliers/' + id, httpOptions).pipe(
            tap(_ => console.log(`deleted supplier id=${id}`)),
            catchError(this.handleError<any>('deleteSupplier'))
        );
    }

}
