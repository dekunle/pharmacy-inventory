import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem("access_token")
  })
};

const endpoint = 'http://localhost:3030/';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private handleErrors(error: any) {
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(error);
}

  getOrders(): Observable<any> {
    return this.http.get(endpoint + 'orders', httpOptions).pipe(
      map(this.extractData));
  }

  getOrder(id): Observable<any> {
    return this.http.get(endpoint + 'orders/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  addOrder(order): Observable<any> {
    // console.log(order);
    return this.http.post<any>(endpoint + 'orders', JSON.stringify(order), httpOptions).pipe(
      tap((result) => console.log(result)),
      catchError(this.handleErrors)
    );
  }

  updateOrder(id, order): Observable<any> {
    return this.http.patch(endpoint + 'orders/' + id, JSON.stringify(order), httpOptions).pipe(
      tap(_ => console.log(`updated order id=${id}`)),
      catchError(this.handleError<any>('updateOrder'))
    );
  }

  deleteOrder(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'orders/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted order id=${id}`)),
      catchError(this.handleError<any>('deleteOrder'))
    );
  }

}
