import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem("access_token")
  })
};

const endpoint = 'http://localhost:3030/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getUsers(): Observable<any> {
    return this.http.get(endpoint + 'users', httpOptions).pipe(
      map(this.extractData));
  }

  getUser(id): Observable<any> {
    return this.http.get(endpoint + 'users/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  addUser(user): Observable<any> {
    // console.log(user);
    return this.http.post<any>(endpoint + 'users', JSON.stringify(user), httpOptions).pipe(
      tap((order) => console.log(`added order w/ id=${user._id}`)),
      catchError(this.handleError<any>('addUser'))
    );
  }

  updateUser(id, user): Observable<any> {
    return this.http.patch(endpoint + 'users/' + id, JSON.stringify(user), httpOptions).pipe(
      tap(_ => console.log(`updated order id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'users/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted user id=${id}`)),
      catchError(this.handleError<any>('deleteUser'))
    );
  }

}
