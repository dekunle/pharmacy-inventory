import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { OrderService } from '../services/order-service';
import { ProductService } from '../services/product-service';
import * as moment from 'moment';
import * as uuid from 'uuid';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {

  @Input() orderData: any = {
    product_id: '',
    discount: '',
    quantity: 1,
    customer_name: '',
    bill_no: uuid.v4(),
    date: moment().format("MMM DD, YYYY"),
    payment_type: '',
    price: 0,
    sold_by: localStorage.getItem('userID')
  };

  products: any = [];
  editOrder: FormGroup;
  submitted: boolean = false;

  public error: string;

  constructor(
    public order_rest: OrderService,
    public product_rest: ProductService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getProducts();
    this.order_rest.getOrder(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.orderData = data;
    });
    this.editOrder = this.formBuilder.group({
      product_id: ['', Validators.required],
      quantity: ['', Validators.required],
      customer_name: ['', Validators.required],
      payment_type: ['', Validators.required]
    });
  }

  getProducts() {
    this.products = [];
    this.product_rest.getProducts().subscribe((data: {}) => {
      console.log(data);
      this.products = data;
    }, (err) => {
      console.log(err);
    });
  }

  updateOrder() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editOrder.invalid) {
      return;
    } else {
      // alert("Submitted");
      this.order_rest.updateOrder(this.route.snapshot.params['id'], this.orderData).subscribe((result) => {
        this.router.navigate(['/orders/']);
      }, (err) => {
        console.log(err);
      });
    }

  }

  fetchProductDetails() {
    const product = this.orderData.product_id;
    this.product_rest.getProduct(product).subscribe((data) => {
      // console.log(data.discount);
      var quantity = this.orderData.quantity
      var price = data.unit_price
      this.orderData.discount = data.discount;
      this.orderData.price = quantity * price;
    }, (err) => {
      console.log(err);
    });
  }

  get form() { return this.editOrder.controls; }

}
