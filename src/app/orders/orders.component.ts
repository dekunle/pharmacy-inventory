import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order-service';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas'; 

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders: any = [];
  p: number = 1;
  total: 0

  constructor(
    private order_rest: OrderService
  ) { }

  getOrders() {
    this.orders = [];
    this.order_rest.getOrders().subscribe((data: {}) => {
      console.log(data);
      this.orders = data;
    }, (err) => {
      console.log(err);
    })
  }

  calculateSum(){
    const total = 0;
    const price = document.getElementById("price");

    // price.each()
  }

  downloadPDF(){
    var data = document.getElementById('salesData');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  

      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('SALES.pdf'); // Generated PDF 
  });
}

  delete(id) {
    this.order_rest.deleteOrder(id)
      .subscribe(res => {
        this.getOrders();
      }, (err) => {
        console.log(err);
      }
      );
  }

  ngOnInit() {
    this.getOrders();
  }

}
