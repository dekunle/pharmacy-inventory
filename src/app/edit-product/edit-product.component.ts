import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../services/product-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SupplierService } from '../services/supplier-service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Input() productData: any = {
    name: '',
    brand_name: '',
    weight: '',
    nafdacno: '',
    category: '',
    unit_price: '',
    discount: '',
    quantity: '',
    description: '',
    supplier_id: ''
  };

  constructor(
    public product: ProductService,
    public supplier_serv: SupplierService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  editProduct: FormGroup;
  submitted: boolean = false;

  public error: string;

  suppliers: any = [];

  ngOnInit() {
    this.getSuppliers();
    this.product.getProduct(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.productData = data;
    });

    this.editProduct = this.formBuilder.group({
      name: ['', Validators.required],
      brand_name: ['', Validators.required],
      weight: ['', Validators.required],
      nafdacno: ['', Validators.required],
      category: ['', Validators.required],
      unit_price: ['', Validators.required],
      discount: ['', Validators.required],
      quantity: [''],
      description: ['', Validators.required],
      supplier_id: ['', Validators.required]
    });
  }

  updateProduct() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editProduct.invalid) {
      return;
    } else {
      this.product.updateProduct(this.route.snapshot.params['id'], this.productData).subscribe((result) => {
        this.router.navigate(['/products/']);
      }, (err) => {
        console.log(err);
      });
    }
  }

  getSuppliers() {
    this.suppliers = [];
    this.supplier_serv.getSuppliers().subscribe((result: {}) => {
      this.suppliers = result
      // console.log(result);
    }, err => {
      console.log(err);
    })
  }

  get form() { return this.editProduct.controls; }

}
