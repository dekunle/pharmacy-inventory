import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../services/supplier-service';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  suppliers: any = [];
  p: number = 1;

  constructor(
    public supplier_serv: SupplierService
  ) { }

  getSuppliers() {
    this.suppliers = [];
    this.supplier_serv.getSuppliers().subscribe((data: {}) => {
      console.log(data);
      this.suppliers = data;
    }, (err) => {
      console.log(err);
    })
  }

  delete(id) {
    this.supplier_serv.deleteSupplier(id)
      .subscribe(res => {
        this.getSuppliers();
      }, (err) => {
        console.log(err);
      }
      );
  }

  ngOnInit() {
    this.getSuppliers();
  }

}
