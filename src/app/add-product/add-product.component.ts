import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ProductService } from '../services/product-service';
import { SupplierService } from '../services/supplier-service';
import * as moment from 'moment';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  @Input() productData = {
    name: '',
    brand_name: '',
    weight: '',
    nafdacno: '',
    category: '',
    unit_price: '',
    discount: '',
    quantity: '',
    description: '',
    supplier_id: '',
    date: moment().format("MMM DD, YYYY")
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public product: ProductService,
    public supplier_serv: SupplierService
  ) { }

  addProduct: FormGroup;
  submitted: boolean = false;

  public hideBtn: boolean = false;
  public error: string;

  suppliers: any = [];

  saveProduct() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addProduct.invalid) {
      return;
    } else {
      // alert("Submitted");
      this.product.addProduct(this.productData).subscribe((result) => {
        this.router.navigate(['/products/']);
      }, (err) => {
        console.log(err);
      });
    }

  }

  getSuppliers() {
    this.suppliers = [];
    this.supplier_serv.getSuppliers().subscribe((result: {}) => {
      this.suppliers = result
    }, err => {
      console.log(err);
    })
  }

  ngOnInit() {
    this.getSuppliers();
    this.addProduct = this.formBuilder.group({
      name: ['', Validators.required],
      brand_name: ['', Validators.required],
      weight: ['', Validators.required],
      nafdacno: ['', Validators.required],
      category: ['', Validators.required],
      unit_price: ['', Validators.required],
      discount: ['', Validators.required],
      quantity: [''],
      description: ['', Validators.required],
      supplier_id: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get form() { return this.addProduct.controls; }

}
