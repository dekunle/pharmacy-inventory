import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth-service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private auth: AuthenticationService
  ) { }

  userData: any;

  ngOnInit() {
    this.userData = localStorage.getItem("fullname");
  }

}
